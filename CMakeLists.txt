cmake_minimum_required(VERSION 3.5)
project(SFT)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O0 -Wall -Wextra -pedantic -std=c++17")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
SET(CMAKE_CXX_COMPILER /usr/bin/clang++)

set(SFT_SOURCE_FILES
    src/sft.cc
    src/socket.cc
    src/log.cc
)

add_executable(client ${SFT_SOURCE_FILES} src/client.cc)
add_executable(server ${SFT_SOURCE_FILES} src/server.cc)

target_link_libraries(client -lstdc++fs -luuid -pthread)
target_link_libraries(server -lstdc++fs -luuid -pthread)
