#include <arpa/inet.h>
#include <libgen.h>

#include <chrono>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <atomic>
#include <netinet/in.h>
#include <thread>
#include <fstream>

#include "log.hh"
#include "sft.hh"
#include "socket.hh"
#include "util.hh"

using namespace sft;
using namespace sft::packets;

class ClientFSM {
public:
    ClientFSM(std::string lfilename, std::string rfilename, UDPSocket& socket, Address const& address) 
    : m_lfilename{std::move(lfilename)}, m_rfilename{std::move(rfilename)}, m_socket{socket},
    m_serverAddress{address}, m_log{sft::Log::getInstance()}, m_accessToken{} {
        m_log.info("Created cfsm\n");
    }

    ClientFSM(ClientFSM const&) = delete;
    ClientFSM(ClientFSM&&) = delete;
    ~ClientFSM() = default;
    ClientFSM& operator=(ClientFSM const&) = delete;
    ClientFSM& operator=(ClientFSM&&) = delete;

    // listen callback
    bool handle([[maybe_unused]] Address&& address, byte const * const buffer, [[maybe_unused]] int const size) {
        m_log.info("Received Packet\n");

        auto header = createFromBytes<MessageHeader>(buffer);
        switch(header.messageType) {
            // TODO Handle Errors better then just printing the message
            case MessageType::error: {
                auto error = createFromBytes<packets::Error>(buffer + sizeof(MessageHeader));
                m_log.err(createMessage(error.message, '\n'));
            }
            case MessageType::endSession: {
                return false;
            }
            case MessageType::packetAck: {
                auto pack = createFromBytes<packets::PacketAck>(buffer + sizeof(MessageHeader));
                if ( pack.accepted ) {

                }
            }
            default:
                break;
        }

        if ( m_state < m_stateFunctions.size() ) {
            return std::invoke(m_stateFunctions[m_state], this, buffer);
        } else {
            m_log.err("Unknown state in ClientFSM\n");
            return false;
        }
    }

    // On close callback
    bool onClose() {
        return true;
    }

    bool login(byte const * const buffer) {
        auto header = createFromBytes<MessageHeader>(buffer);
        if(header.messageType != MessageType::loginResponse) {
            return false;
        }

        m_accessToken = UUID{header.accessToken};
        m_log.info(createMessage("Access Token: ", m_accessToken.parse(), '\n'));

        auto rfilename = new char[m_rfilename.size()];
        memcpy(rfilename, m_rfilename.c_str(), m_rfilename.size());

        auto sftr = Request{};
        strcpy(reinterpret_cast<char*>(sftr.filename), basename(rfilename));
        strcpy(reinterpret_cast<char*>(sftr.filepath), dirname(rfilename));
        sftr.filesize = 0;

        auto sendBuffer = createPacket(sftr, m_accessToken);

        if(!m_socket.sendTo(m_serverAddress, sendBuffer.data(), sendBuffer.size())) {
            m_log.err(createMessage("Error sending response packet \"", strerror(errno), "\"\n"));
            return false;
        }

        m_state++;
        return true;
    }

    bool onRequestAccept(byte const * buffer) {
        if(!checkHeader(&buffer, MessageType::requestResponse)) return false;

        auto sftrr = createFromBytes<RequestResponse>(buffer);
        if(!sftrr.accepted) {
            m_log.warn("Packet request rejected\n");       
            return false;
        }

        m_buffer.resize(sftrr.blockSize);

        m_filestream = std::ifstream{m_lfilename, std::ios::binary};
        if(!m_filestream.is_open()) return false;

        auto res = sendFileBlock();
        if(!res) return false;
        m_state += m_filestream.eof() ? 2 : 1;
        return true; 
    }

    bool sendFileBlocks(byte const * buffer) {
        if(!checkHeader(&buffer, MessageType::packetAck)) return false;

        auto res = sendFileBlock();
        if(!res) return false;
        if(m_filestream.eof()) {
            m_state++;
        }

        return true; 
    }

    bool sendFileBlock() {
        auto p = Packet{};
        p.offset = m_filestream.tellg();

        if(!m_filestream.read(reinterpret_cast<char*>(p.data), sizeof(p.data)) && !m_filestream.eof()) {
            return false;
        }
        p.size = m_filestream.gcount();
        
        auto sendBuffer = createPacket(p, m_accessToken);

        if(!m_socket.sendTo(m_serverAddress, sendBuffer.data(), sendBuffer.size())) {
            m_log.err(createMessage("Error sending file block packet\"", strerror(errno), "\"\n"));
            return false;
        }

        return true;
    }

    bool sendFileCompleted([[maybe_unused]] byte const* buffer) {
        auto sftes = EndSession{};
        auto sendBuffer = createPacket(sftes, m_accessToken);
        if(!m_socket.sendTo(m_serverAddress, sendBuffer.data(), sendBuffer.size())) {
            m_log.err(createMessage("Error sending end session packet\"", strerror(errno), "\"\n"));
            return false;
        }
        return true;
    }

    [[nodiscard]] bool checkHeader(byte const ** buffer, MessageType mt) {
        auto header = createFromBytes<MessageHeader>(*buffer);
        *buffer += sizeof(MessageHeader);
        return header.messageType == mt && UUID{header.accessToken} == m_accessToken;
    }

private:
    std::string const m_lfilename;
    std::string const m_rfilename;
    UDPSocket const& m_socket;
    Address const& m_serverAddress;
    sft::Log& m_log;

    UUID m_accessToken;
    std::ifstream m_filestream;
    std::vector<byte> m_buffer;
    int m_state = 0;

    constexpr static std::array m_stateFunctions{
        &ClientFSM::login,
        &ClientFSM::onRequestAccept,
        &ClientFSM::sendFileBlocks,
        &ClientFSM::sendFileCompleted
    };
};

void help() {
    std::cout << "Usage: client.out [source filename] [dest filename]\n";
}

int main(int argc, char** argv) {
    sft::Log::createInstance(std::cout, std::cerr);

    std::cout << "SFT v" << sft::parseVersion(sft::getVersion()) << '\n';
    if(argc != 3) {
        help();
        std::exit(-1);
    }

    UDPSocket sock{};
    sock.attachPreSendHook(sft::CRCPreSendHook);
    sock.attachPostSendHook(sft::CRCPostSendHook);

    auto servaddr = Address{"127.0.0.1", SERVER_PORT};
    auto cfsm = ClientFSM{argv[1], argv[2], sock, servaddr};
    auto clientSock = UDPSocket{{"127.0.0.1", CLIENT_PORT}};

    clientSock.attachPreReceiveHook(sft::CRCPreReceiveHook);
    clientSock.bind();
    clientSock.listenAsync(
        [&cfsm] (auto addr, auto buff, auto size) { return cfsm.handle(std::move(addr), buff, size); }, 
        [&cfsm] () { return cfsm.onClose(); }, 
        8096
    );

    Login sftl{"username", "password"};
    auto sftlv = createPacket(sftl);

    sock.sendTo(servaddr, sftlv.data(), sftlv.size());

    clientSock.join();

    return 0;
}
