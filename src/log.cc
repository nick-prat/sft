#include "log.hh"
#include <stdexcept>

namespace {
    [[maybe_unused]] constexpr char INFO_TAG[] = "[[INFO]] ";
    [[maybe_unused]] constexpr char DEBUG_TAG[] = "[[DEBUG]] ";
    [[maybe_unused]] constexpr char WARN_TAG[] = "[[WARNING]] ";
    [[maybe_unused]] constexpr char ERROR_TAG[] = "[[ERROR]] ";
}

void sft::Log::createInstance(std::ostream& out, std::ostream& eout) {
    if ( m_log != nullptr ) {
        delete m_log;
    }

    m_log = new Log(out, eout);
}

sft::Log& sft::Log::getInstance() {
    if ( m_log == nullptr ) {
        throw std::runtime_error{"No log instance available"};
    }

    return *m_log;
}

void sft::Log::log(std::string_view str) {
    m_output.write(str.data(), str.length());
}

void sft::Log::info(std::string_view str) {
    m_output.write(INFO_TAG, sizeof(INFO_TAG));
    m_output.write(str.data(), str.size());
}

void sft::Log::debug(std::string_view str) {
    m_output.write(DEBUG_TAG, sizeof(DEBUG_TAG));
    m_output.write(str.data(), str.size());
}

void sft::Log::warn(std::string_view str) {
    m_eoutput.write(DEBUG_TAG, sizeof(DEBUG_TAG));
    m_eoutput.write(str.data(), str.size());
}

void sft::Log::err(std::string_view str) {
    m_output.write(ERROR_TAG, sizeof(ERROR_TAG));
    m_eoutput.write(str.data(), str.length());
}

sft::Log::Log(std::ostream& out, std::ostream& eout)
: m_output{out}, m_eoutput{eout} {}
