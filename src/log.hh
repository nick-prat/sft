#ifndef SFT_LOG_HH
#define SFT_LOG_HH

#include <sstream>
#include <string_view>

namespace sft {

    template<typename... Ts>
    std::string createMessage(Ts&&... args) {
        auto ss = std::stringstream{};
        (ss << ... << args) << '\n';
        return ss.str();
    }

    class Log {
    public:
        static void createInstance(std::ostream& out, std::ostream& eout);
        static Log& getInstance();

        void log(std::string_view str);
        void info(std::string_view str);
        void debug(std::string_view str);
        void warn(std::string_view str);
        void err(std::string_view str);

    private:
        Log(std::ostream& out, std::ostream& eout);

        std::ostream& m_output;
        std::ostream& m_eoutput;
        inline static Log* m_log = nullptr;
    };
}
 

#endif // SFT_LOG_HH