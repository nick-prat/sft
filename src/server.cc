#include <arpa/inet.h>
#include <iostream>
#include <netinet/in.h>

#include "socket.hh"
#include "sft.hh"

using namespace sft;

int main() {
    sft::Log::createInstance(std::cout, std::cerr);

    std::cout << "SFT v" << sft::parseVersion(sft::getVersion()) << '\n';
    SessionManager sm{{{"127.0.0.1", SERVER_PORT}}};
    sm.listen();

    return 0;
}