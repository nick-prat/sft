#include <chrono>
#include <iterator>
#include <stdexcept>
#include <sstream>
#include <filesystem>
#include <iostream>
#include <iomanip>
#include <thread>
#include <utility>
#include <unordered_set>

#include "log.hh"
#include "util.hh"
#include "sft.hh"
#include "socket.hh"

using namespace sft;
using namespace sft::packets;

// Generate and add CRC to end of message
// TODO Include MessageTail in CRC, possibly set CRC to 0, then calculate.
bool sft::CRCPreSendHook(byte const** data, int& size) {
    auto buffer = new byte[size];
    memcpy(buffer, *data, size);

    auto tail = reinterpret_cast<MessageTail*>(buffer + size - sizeof(MessageTail));
    tail->crc = sft::generateCRC(buffer, size - sizeof(MessageTail));
    
    sft::Log::getInstance().info(createMessage(tail->crc));

    *data = buffer;
    return true;
}

void sft::CRCPostSendHook(byte const* data, [[maybe_unused]] int& size) {
    delete [] data;
}

bool sft::CRCPreReceiveHook(byte const** data, int& size) {
    auto crc = sft::generateCRC(*data, size - sizeof(MessageTail));

    auto tail = reinterpret_cast<MessageTail const*>(*data + size - sizeof(MessageTail));

    sft::Log::getInstance().info(createMessage(crc, ':', tail->crc));
    return crc == tail->crc;
}

uint16_t sft::encodeVersion(uint8_t major, uint8_t minor) {
    uint16_t version = major;
    return (version << 8) | minor;
}

std::tuple<uint8_t, uint8_t> decodeVersion(uint16_t version) {
    uint8_t major = version >> 8;
    uint8_t minor = version & 0x00FF;
    return {major, minor};
}

std::string sft::parseVersion(uint16_t version) {
    auto ss = std::stringstream{};
    ss << (version >> 8) << '.' << (version & 0xFF);
    return ss.str();
}

sft::UUID::UUID() {
    uuid_generate(m_uuid);
}

sft::UUID::UUID(uuid_t uuid) {
    memcpy(m_uuid, uuid, sizeof(uuid_t));
}

uuid_t const* sft::UUID::getUUID() const {
    return &m_uuid;
}

std::string sft::UUID::parse() const {
    auto ss = std::stringstream{};
    auto uuid = reinterpret_cast<uint8_t const*>(&m_uuid);
    ss << std::hex << std::setfill('0');
    ss << std::setw(2) << static_cast<int>(uuid[0]) << std::setw(2) << static_cast<int>(uuid[1]) << std::setw(2) << static_cast<int>(uuid[2]) << std::setw(2) << static_cast<int>(uuid[3]) << '-';
    ss << std::setw(2) << static_cast<int>(uuid[4]) << std::setw(2) << static_cast<int>(uuid[5]) << '-';
    ss << std::setw(2) << static_cast<int>(uuid[6]) << std::setw(2) << static_cast<int>(uuid[7]) << '-';
    ss << std::setw(2) << static_cast<int>(uuid[8]) << std::setw(2) << static_cast<int>(uuid[9]) << '-';
    ss << std::setw(2) << static_cast<int>(uuid[10]) << std::setw(2) << static_cast<int>(uuid[11]) << std::setw(2) << static_cast<int>(uuid[12]);
    ss << std::setw(2) << static_cast<int>(uuid[13]) << std::setw(2) << static_cast<int>(uuid[14]) << std::setw(2) << static_cast<int>(uuid[15]);
    return ss.str();
}

bool sft::UUID::operator==(UUID const& other) const {
    return memcmp(m_uuid, other.m_uuid, sizeof(uuid_t)) == 0;
}

sft::ServerSession::ServerSession(UUID accessToken, UDPSocket&& socket) 
: m_socket{{socket.getHostName(), CLIENT_PORT}}, m_accessToken{accessToken}, m_done{false} {
    m_socket.attachPreSendHook(sft::CRCPreSendHook);
    m_socket.attachPostSendHook(sft::CRCPostSendHook);
}

sft::ServerSession::~ServerSession() {
    if(std::filesystem::exists(m_filePath) && !m_done) {
        std::filesystem::remove(m_filePath);
    }
}

void sft::ServerSession::handlePacket(Request const& sftr) {
    auto ss = std::stringstream{};    
    ss << sftr.filepath << std::filesystem::path::preferred_separator << sftr.filename;
    m_filePath = std::filesystem::path{ss.str()};

    auto sftrr = RequestResponse{};
    if (std::filesystem::exists(m_filePath)) {
        sftrr.accepted = false;
    } else {
        if (m_file = std::ofstream{m_filePath}; m_file.is_open()) {
            std::filesystem::resize_file(m_filePath, sftr.filesize);
            sftrr.accepted = true;
            sftrr.blockSize = detail::blockSize;
        } else {
            sftrr.accepted = false;
        }
    }

    auto msg = createPacket(sftrr, m_accessToken);
    m_socket.send(msg.data(), msg.size());
}

void sft::ServerSession::handlePacket([[maybe_unused]] Login const& sftl) {
    auto sftlr = LoginResponse{};
    auto msg = createPacket(sftlr, m_accessToken);
    m_socket.send(msg.data(), msg.size());
}

void sft::ServerSession::handlePacket(Packet const& sftp) {
    m_file.seekp(sftp.offset);
    m_file.write(reinterpret_cast<char const*>(sftp.data), sftp.size);

    auto msg = createPacket(PacketAck{}, m_accessToken);
    m_socket.send(msg.data(), msg.size());
}

void sft::ServerSession::handlePacket([[maybe_unused]] EndSession const& sftes) {
    auto msg = createPacket(EndSession{}, m_accessToken);
    m_socket.send(msg.data(), msg.size());
    m_done = true;
}

void sft::ServerSession::sendError(std::string const &msg) {
    auto error = Error{};
    strcpy(error.message, msg.c_str());

    auto packet = createPacket(error, m_accessToken);
    m_socket.send(packet.data(), msg.size());
}

sft::SessionManager::SessionManager(UDPSocket&& socket)
: m_socket{std::move(socket)}, m_log{sft::Log::getInstance()}, m_stopToken{false} {
    if(!m_socket.setTimeout(1)) {
        throw std::runtime_error{"couldn't set socket to non blocking\n"};
    }

    m_socket.attachPreSendHook(sft::CRCPreSendHook);
    m_socket.attachPostSendHook(sft::CRCPostSendHook);
    m_socket.attachPreReceiveHook(sft::CRCPreReceiveHook);

    m_timeStampThread = std::thread([this]() {
        while(!m_stopToken.load()) {
            m_log.info("Checking timestamps\n");
            checkTimeStamps();
            std::this_thread::sleep_for(std::chrono::seconds(60));
        }
    });
}

sft::SessionManager::~SessionManager() {
    m_stopToken = true;
    if(m_timeStampThread.joinable()) {
        m_timeStampThread.join();
    }
}

void sft::SessionManager::listen() {
    if(!m_socket.isBound()) {
        m_socket.bind();
    }
    
    m_socket.listen([this](Address&& address, byte const* buffer, [[maybe_unused]] int size) {
        handlePacket(std::move(address), buffer);
        return true;
    }, 8096);

    m_socket.unbind();
}

bool sft::SessionManager::createSession(UUID const accessToken, UDPSocket socket) {
    if(m_sessions.find(accessToken) != m_sessions.end()) return false;
    
    auto addr = Address{socket.getHostNameRaw(), socket.getPort()};

    auto [iter, placed] = m_sessions.emplace(accessToken, ServerSession{accessToken, std::move(socket)});
    if(placed) {
        auto [iter, placed] = m_tokens.emplace(addr, accessToken);
        if(!placed) {
            m_sessions.erase(accessToken);
            return false;
        }
    } else {
        return false;
    }

    updateTimeStamp(addr);
    m_log.info(createMessage("Created session for ", addr, " with token ", accessToken.parse()));
    return true;
}

void sft::SessionManager::closeSession(Address const& address) {
    try {
        auto accessToken = m_tokens.at(address);
        m_tokens.erase(address);
        m_sessions.erase(accessToken);
        m_log.info(createMessage("Closed session for ", address, " with token ", accessToken.parse()));
    } catch(std::out_of_range& err) {
        m_log.err(createMessage("closeSession attempted on non existant address: ", address));
    }
}

void sft::SessionManager::handlePacket(Address&& address, byte const* buffer) {
    m_log.info(createMessage("Package received from ", address));

    auto messageHeader = createFromBytes<MessageHeader>(buffer);
    buffer += sizeof(MessageHeader);

    if(messageHeader.messageType != MessageType::login) {
        if(verifyAccessToken(address, messageHeader.accessToken)) {
            updateTimeStamp(address);
        } else {
            m_log.warn(createMessage("Address and access token mismatch for [", address, ':', UUID{messageHeader.accessToken}.parse(), ']'));
            return;
        }
    }

    switch(messageHeader.messageType) {
        case MessageType::login: {
            auto sftl = createFromBytes<Login>(buffer);
            if(verifyLogin(sftl)) {
                auto hostNamePort = address.getHostNamePort();
                auto accessToken = UUID{};
                if(!createSession(accessToken, address)) {
                    m_log.err(createMessage("Failed to create session for ", hostNamePort));
                    auto packet = createPacket(Error{"Access Token Duplicate Error"});
                    m_socket.sendTo({address.getHostNameRaw(), CLIENT_PORT}, packet.data(), packet.size());
                    break;
                }
                routePacket(accessToken, sftl);
            } else {
                auto packet = createPacket(Error{"Login Failed"});
                m_socket.sendTo({address.getHostName(), CLIENT_PORT}, packet.data(), packet.size());
                break;
            }
            break;
        }
        case MessageType::request: {
            routePacket(messageHeader.accessToken, createFromBytes<Request>(buffer));
            break;
        }
        case MessageType::endSession: {
            routePacket(messageHeader.accessToken, createFromBytes<EndSession>(buffer));
            closeSession(address);
            m_log.info(createMessage("Closed session for ", address));
            break;
        }
        case MessageType::packet: {
            routePacket(messageHeader.accessToken, createFromBytes<Packet>(buffer));
            break;
        }
        default: {
            m_log.warn("Unknown message type\n");
            break;
        }
    }
}

bool sft::SessionManager::verifyLogin(packets::Login const& sftl) {
    auto ifstream = std::ifstream{"/usr/share/sft/login"};
    if(!ifstream.is_open()) {
        return false;
    }

    auto username = std::string{};
    ifstream >> username;
    auto password = std::string{};
    ifstream >> password;

    auto res = strcmp(username.c_str(), reinterpret_cast<char const*>(sftl.username)) == 0 
            && strcmp(password.c_str(), reinterpret_cast<char const*>(sftl.password)) == 0;

    if(res == false) {
        m_log.warn(createMessage("Login ", sftl.username, ":", sftl.password, " rejected"));
    }
    return res;
}

bool sft::SessionManager::verifyAccessToken(Address const& address, UUID const& accessToken) const {
    try {
        return accessToken == m_tokens.at(address);
    } catch(std::out_of_range const& error) {
        return false;
    }
}

void sft::SessionManager::checkTimeStamps() {
    auto l1 = std::lock_guard(m_timeStampMutex);

    auto timeout = std::chrono::system_clock::now() - std::chrono::seconds(60);

    auto addrs = std::unordered_set<Address>{};
    for(auto& [addr, stamp] : m_timeStamps) {
        if (stamp < timeout) {
            addrs.emplace(addr);
        }
    }

    if(!addrs.empty()) {
        auto l2 = std::lock_guard(m_sessionMutex);
        for(auto& addr : addrs) {
            m_timeStamps.erase(addr);
            closeSession(addr);
        }
    }

}

void sft::SessionManager::updateTimeStamp(Address const& addr) {
    auto lock = std::lock_guard(m_timeStampMutex);
    m_timeStamps[addr] = std::chrono::system_clock::now();
}

// XMODEM CRC
CRC sft::generateCRC(byte const* const buffer, int const size) {
    sft::Log::getInstance().info(createMessage("Creating CRC from ", size, ", first byte: ", static_cast<int>(*buffer)));
    CRC poly = 0x1021;
    CRC crc = 0x0000;
    for(int i = 0; i < size; i++) {
        crc = crc ^ (buffer[i] << 8);
        for(int j = 0; j < 8; j++) {
            if((crc & 0x8000) != 0)
                crc = (crc << 1) ^ poly;
            else
                crc <<= 1;
        }
    }
    return crc;
}