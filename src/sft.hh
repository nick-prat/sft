#ifndef SFT_HH
#define SFT_HH

#include <uuid/uuid.h>

#include <cstring>

#include <mutex>
#include <chrono>
#include <fstream>
#include <filesystem>
#include <unordered_map>

#include "socket.hh"
#include "log.hh"

namespace sft {
    inline constexpr uint8_t majorVersion = 0;
    inline constexpr uint8_t minorVersion = 1;

    constexpr uint16_t getVersion() {
        return (static_cast<uint16_t>(majorVersion) << 8) | minorVersion;
    }

    bool CRCPreSendHook(byte const** data, int& size);
    void CRCPostSendHook(byte const* data, int& size);
    bool CRCPreReceiveHook(byte const** data, int& size);

    uint16_t encodeVersion(uint8_t major, uint8_t minor);
    std::tuple<uint8_t, uint8_t> decodeVersion(uint16_t version);
    std::string parseVersion(uint16_t);

    class UUID {
    public:
        UUID();
        UUID(uuid_t uuid);
        UUID(UUID const&) = default;
        UUID(UUID&&) = default;
        ~UUID() = default;

        UUID& operator=(UUID const&) = default;
        UUID& operator=(UUID &&) = default;

        bool operator==(UUID const& other) const;

        [[nodiscard]] uuid_t const* getUUID() const;
        [[nodiscard]] std::string parse() const;

    private:
        uuid_t m_uuid;
    };
}

namespace std {
    template<>
    struct hash<sft::UUID> {
        std::size_t operator()(sft::UUID const& uuid) const {
            return std::hash<std::string>{}(uuid.parse());
        }
    };
}

namespace sft {

    namespace detail {
        constexpr int blockSize = 4096;
    }

    using CRC = uint16_t;

    namespace packets {
        enum class MessageType : byte {
            login = 0x1,
            loginResponse = 0x2,
            request,
            requestResponse,
            packet,
            packetAck,
            endSession,
            error
        };

        struct MessageHeader {
            MessageType messageType;
            uuid_t accessToken;
        };

        struct MessageTail {
            CRC crc;
        };

        struct Login {
            byte username[256];
            byte password[256];
        };

        struct LoginResponse {
            uint16_t version;
        };

        struct Request {
            int32_t filesize;
            byte filepath[4096];
            byte filename[255];
            byte padding;
        };

        struct RequestResponse {
            bool accepted;
            uint16_t blockSize;
        };

        struct Packet {
            int offset;
            uint16_t size;
            byte data[detail::blockSize];
        };

        struct PacketAck {
            bool accepted;
        };

        struct EndSession {
        };

        struct Error {
            char message[4096];
        };
    };

    class SessionManager;

    class ServerSession {
    public:
        ServerSession(UUID accessToken, UDPSocket&& socket);
        ServerSession(ServerSession&& session) = default;
        ServerSession(ServerSession const& session) = delete;
        ~ServerSession();

        ServerSession& operator=(ServerSession&& session) = delete;
        ServerSession& operator=(ServerSession const& session) = delete;

        void handlePacket(packets::Request const& sftr);
        void handlePacket(packets::Login const& sftl);
        void handlePacket(packets::Packet const& sftp);
        void handlePacket(packets::EndSession const& sftes);

    private:
        void sendError(std::string const& msg);

    private:
        UDPSocket m_socket;
        UUID m_accessToken;
        
        bool m_done;
        std::filesystem::path m_filePath;
        std::ofstream m_file;
    };

    class SessionManager {
    public:
        SessionManager(UDPSocket&& socket);
        SessionManager(SessionManager const&) = delete;
        SessionManager(SessionManager&&) = delete;
        ~SessionManager();

        SessionManager& operator=(SessionManager const&) = delete;
        SessionManager& operator=(SessionManager&&) = delete;

        void listen();

    private:
        bool createSession(UUID accessToken, UDPSocket socket);
        void closeSession(Address const& address);

        void handlePacket(Address&& address, byte const* const buffer);

        template<typename T>
        void routePacket(UUID const& token, T const& packet) {
            auto lock = std::lock_guard(m_sessionMutex);
            auto iter = m_sessions.find(token);
            if(iter != m_sessions.end()) {
                iter->second.handlePacket(packet);
            }
        }

        bool verifyLogin(packets::Login const& sftl);
        bool verifyAccessToken(Address const& address, UUID const& accessToken) const;

        void checkTimeStamps();
        void updateTimeStamp(Address const& addr);

    private:
        UDPSocket m_socket;
        sft::Log& m_log;

        std::mutex m_timeStampMutex, m_sessionMutex;
        std::thread m_timeStampThread;
        std::atomic<bool> m_stopToken;
        std::unordered_map<Address, std::chrono::system_clock::time_point> m_timeStamps;
        std::unordered_map<Address, UUID> m_tokens;
        std::unordered_map<UUID, ServerSession> m_sessions;
    };

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::Login const& msg) {
        return packets::MessageType::login;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::LoginResponse const& msg) {
        return packets::MessageType::loginResponse;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::Request const& msg) {
        return packets::MessageType::request;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::RequestResponse const& msg) {
        return packets::MessageType::requestResponse;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::Packet const& msg) {
        return packets::MessageType::packet;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::PacketAck const& msg) {
        return packets::MessageType::packetAck;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::EndSession const& msg) {
        return packets::MessageType::endSession;
    }

    [[nodiscard]] constexpr packets::MessageType getMessageType([[maybe_unused]] packets::Error const& msg) {
        return packets::MessageType::error;
    }

    template<typename T>
    std::vector<byte> createPacket(T const& sftm, UUID uuid = {}) {
        auto size = sizeof(packets::MessageHeader) + sizeof(T) + sizeof(packets::MessageTail);
        auto vbuffer = std::vector<byte>{};
        vbuffer.resize(size);
        auto buffer = vbuffer.data();

        packets::MessageHeader sftmh{getMessageType(sftm), {0}};
        memcpy(sftmh.accessToken, uuid.getUUID(), sizeof(uuid_t));
        memcpy(buffer, &sftmh, sizeof(packets::MessageHeader));
        memcpy(buffer + sizeof(packets::MessageHeader), &sftm, sizeof(T));

        return vbuffer;
    }

    CRC generateCRC(byte const* const buffer, int const size);
};

// constexpr bool checkFileSize() {
//     if (sizeof(SFTLogin) != 512) return false;
//     if (sizeof(SFTLoginResponse) != 8) return false;
//     if (sizeof(SFTRequest) != 4356) return false;

//     return true;
// }

// static_assert(checkFileSize(), "File sizes are incorrect");

#endif // SFT_HH