#include <asm-generic/errno-base.h>
#include <asm-generic/errno.h>
#include <asm-generic/socket.h>
#include <bits/stdint-uintn.h>
#include <bits/types/struct_timeval.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sstream>

#include "log.hh"
#include "socket.hh"

using SockAddr = struct sockaddr;

namespace {
    sockaddr_in generateSockAddr(uint32_t const ip, uint16_t const port) {
        sockaddr_in sockaddr{};
        sockaddr.sin_family = AF_INET;
        sockaddr.sin_port = htons(port);
        sockaddr.sin_addr.s_addr = htonl(ip);
        return sockaddr;
    }

    [[nodiscard]] Address generateAddress(sockaddr_in const& sockaddr) {
        return {ntohl(sockaddr.sin_addr.s_addr), ntohs(sockaddr.sin_port)};
    }
}

Address::Address()
: m_port{0} {}

Address::Address(std::string const& ip, uint16_t const port)
: m_port{port} {
    auto in = in_addr{};
    inet_aton(ip.c_str(), &in);
    m_ip = ntohl(in.s_addr);
}

Address::Address(uint32_t const ip, uint16_t const port)
: m_ip{ip}, m_port{port} {}

Address::Address(Address&& address) noexcept
: m_ip{address.m_ip}, m_port{address.m_port} {
    address.m_ip = 0;
    address.m_port = 0;
}

bool Address::operator==(Address const& address) const {
    return getHostNamePort() == address.getHostNamePort();
}

std::string Address::getHostName() const {
    return inet_ntoa(in_addr{htonl(m_ip)});
}

uint32_t Address::getHostNameRaw() const {
    return m_ip;
}

uint16_t Address::getPort() const {
    return m_port;
}

std::string Address::getHostNamePort() const {
    std::stringstream ss;
    ss << getHostName() << ':' << m_port;
    return ss.str();
}

bool Address::isValid() const {
    return m_ip != 0 && m_port != 0;
}

UDPSocket::UDPSocket() {
    createSocketFD();
}

UDPSocket::UDPSocket(Address&& address)
: Address{std::move(address)} {
    createSocketFD();
}

UDPSocket::UDPSocket(Address const& address)
: Address{address} {
    createSocketFD();
}

UDPSocket::UDPSocket(UDPSocket&& socket) noexcept
: Address{std::move(socket)}, m_sockfd{socket.m_sockfd}
, m_preSendHooks{std::move(socket.m_preSendHooks)}, m_postSendHooks{std::move(socket.m_postSendHooks)}
, m_preReceiveHooks{std::move(socket.m_preReceiveHooks)}, m_postReceiveHooks{std::move(socket.m_postReceiveHooks)}
, m_stopToken{socket.m_stopToken.load()}, m_thread{std::move(socket.m_thread)} {
    socket.m_sockfd = -1;
}

UDPSocket::~UDPSocket() {
    std::cout << "closing socket\n";
    unbind();
}

bool UDPSocket::bind() {
    auto sockadd = generateSockAddr(m_ip, m_port);
    if(m_sockfd == -1) {
        createSocketFD();
    }
    std::cout << "Binding " << getHostNamePort() << '\n';
    int val = ::bind(m_sockfd, reinterpret_cast<SockAddr*>(&sockadd), sizeof(sockaddr_in));
    return val == 0;
}

bool UDPSocket::unbind() {
    if(m_sockfd == -1) return true;
    if(close(m_sockfd) !=  0) return false;
    m_sockfd = -1;
    return true;
}

bool UDPSocket::rebind() {
    if(!unbind()) return false;
    if(!bind()) return false;
    return true;
}

bool UDPSocket::isBound() const {
    sockaddr_in sa;
    auto sa_len = sizeof(sa);
    if (getsockname(m_sockfd, reinterpret_cast<sockaddr*>(&sa), reinterpret_cast<socklen_t*>(&sa_len)) == -1) {
        throw std::runtime_error{"getsockname call failed"};
    }

    Address addr{ntohl(sa.sin_addr.s_addr), ntohs(sa.sin_port)};
    return addr.isValid();
}

bool UDPSocket::isBlocking() const {
    auto tv = timeval{};
    auto size = sizeof(timeval);
    getsockopt(m_sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, reinterpret_cast<unsigned int*>(&size));
    std::cout << tv.tv_sec << '\n';
    return tv.tv_sec != 0 || tv.tv_usec != 0;
}

bool UDPSocket::listen(std::function<bool(Address&&, byte const*, int)> func, int const bufferSize) const {
    auto buffer = new byte[bufferSize];
    sockaddr_in sockaddr{};
    unsigned int length = sizeof(sockaddr_in);

    int received = 0;
    for(;;) {
        memset(buffer, 0, bufferSize);
        // TODO: Handle errors and proper disconnects
        if((received = recvfrom(m_sockfd, buffer, bufferSize,  MSG_WAITALL, reinterpret_cast<SockAddr*>(&sockaddr), &length)) > 0) {
            byte const* bptr = buffer;
            for(auto& func : m_preReceiveHooks) {
                if(!func(&bptr, received)) {
                    sft::Log::getInstance().info("Prereceive hook return false, exiting listen loop\n");
                    return false;
                }
            }
            if(!func(generateAddress(sockaddr), bptr, received)) {
                sft::Log::getInstance().info("Callback return false, exiting listen loop\n");
                return false;
                break;
            }
            for(auto& func : m_postReceiveHooks) {
                sft::Log::getInstance().info("Postreceive hook return false, exiting listen loop\n");
                func(bptr, received);
            }
        } else if(errno != EWOULDBLOCK) {
            break;
        }
    }

    delete [] buffer;
    return true;
}

void UDPSocket::listenAsync(std::function<bool(Address&&, byte const*, int)> func, std::function<bool()> onClose, int const bufferSize) const {
    m_thread = std::thread{[this, &func, &onClose, bufferSize]() {
        while(listen(std::forward<decltype(func)>(func), bufferSize));
        onClose();
    }};
}

void UDPSocket::join() const {
    if(m_thread.joinable()) {
        m_thread.join();
        m_thread = std::thread{};
    }
}

bool UDPSocket::sendTo(Address const& sock, byte const* buffer, int size) const {
    for(auto& func : m_preSendHooks) {
        if(!func(&buffer, size)) return false;
    }
    std::cout << "Sending " << size << " bytes to " << sock.getHostNamePort() << '\n';
    auto sockaddr = generateSockAddr(sock.getHostNameRaw(), sock.getPort());
    auto result = sendto(m_sockfd, buffer, size, MSG_CONFIRM, reinterpret_cast<SockAddr*>(&sockaddr), sizeof(sockaddr_in)) != -1;

    for(auto& func : m_postSendHooks) {
        func(buffer, size);
    }
    return result;
}

bool UDPSocket::send(byte const* const buffer, int const size) const {
    return sendTo(*this, buffer, size);
}

bool UDPSocket::setTimeout(int const timeout) {
    timeval tv{};
    tv.tv_sec = timeout;
    return setsockopt(m_sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(timeval)) == 0;
}

void UDPSocket::attachPreSendHook(std::function<bool(byte const**, int&)> hook) {
    m_preSendHooks.emplace_back(std::move(hook));
}

void UDPSocket::attachPostSendHook(std::function<void(byte const*, int&)> hook) {
    m_postSendHooks.emplace_back(std::move(hook));
}

void UDPSocket::attachPreReceiveHook(std::function<bool(byte const**, int&)> hook) {
    m_preReceiveHooks.emplace_back(std::move(hook));
}

void UDPSocket::attachPostReceiveHook(std::function<void(byte const*, int&)> hook) {
    m_postReceiveHooks.emplace_back(std::move(hook));
}

void UDPSocket::createSocketFD() {
    m_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(m_sockfd < 0) {
        throw std::runtime_error{"Couldn't initialize UDP socket"};
    }
}

std::ostream& operator<<(std::ostream& os, Address const& addr) {
    os << addr.getHostNamePort();
    return os;
}