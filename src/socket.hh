#ifndef SOCKET_HH
#define SOCKET_HH

#include <atomic>
#include <string>
#include <thread>
#include <functional>

using byte = uint8_t;

constexpr inline static int SERVER_PORT = 4080;
constexpr inline static int CLIENT_PORT = 4081;

class Address {
public:
    Address();
    Address(std::string const& ip, uint16_t const port);
    Address(uint32_t const ip, uint16_t const port);
    Address(Address const& address) = default;
    Address(Address&& address) noexcept;
    virtual ~Address() = default;

    [[nodiscard]] bool operator==(Address const& address) const;

    [[nodiscard]] std::string getHostName() const;
    [[nodiscard]] uint32_t getHostNameRaw() const;
    [[nodiscard]] uint16_t getPort() const;
    [[nodiscard]] std::string getHostNamePort() const;
    [[nodiscard]] bool isValid() const;

protected:
    uint32_t m_ip;
    uint16_t m_port;
};

class UDPSocket : public Address {
public:
    UDPSocket();
    UDPSocket(Address const& address);
    UDPSocket(Address&& address);
    UDPSocket(UDPSocket const& socket) = delete;
    UDPSocket(UDPSocket&& socket) noexcept;
    ~UDPSocket() override;

    bool bind();
    bool unbind();
    bool rebind();
    [[nodiscard]] bool isBound() const;
    [[nodiscard]] bool isBlocking() const;

    bool listen(std::function<bool(Address&&, byte const*, int)> func, int const bufferSize) const;
    void listenAsync(std::function<bool(Address&&, byte const*, int)> func, std::function<bool()> onClose, int const bufferSize) const;
    void join() const;
    bool sendTo(Address const& sock, byte const* buffer, int size) const;
    bool send(byte const* buffer, int size) const;
    bool setTimeout(int const timeout);

    void attachPreSendHook(std::function<bool(byte const**, int&)> hook);
    void attachPostSendHook(std::function<void(byte const*, int&)> hook);
    void attachPreReceiveHook(std::function<bool(byte const**, int&)> hook);
    void attachPostReceiveHook(std::function<void(byte const*, int&)> hook);

private:
    void createSocketFD();

private:    
    int m_sockfd;
    std::vector<std::function<bool(byte const**, int&)>> m_preSendHooks;
    std::vector<std::function<void(byte const*, int&)>> m_postSendHooks;
    std::vector<std::function<bool(byte const**, int&)>> m_preReceiveHooks;
    std::vector<std::function<void(byte const*, int&)>> m_postReceiveHooks;
    std::atomic<bool> m_stopToken;
    std::thread mutable m_thread;
};

class CRCSocket : public UDPSocket {
public:
    bool listen(std::function<bool(Address&&, byte*, int)> func, int const bufferSize) const;
};

std::ostream& operator<<(std::ostream& os, Address const& addr);

namespace std {
    template<>
    struct hash<Address> {
        std::size_t operator()(Address const& address) const {
            return std::hash<std::string>{}(address.getHostNamePort());
        }
    };
}

#endif // SOCKET_HH