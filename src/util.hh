#ifndef UTIL_HH
#define UTIL_HH

#include <cstring>

template<typename T>
T createFromBytes(unsigned char const* const buffer) {
    T t{};
    memcpy(&t, buffer, sizeof(T));
    return t;
}

#endif // UTIL_HH